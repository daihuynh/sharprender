﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpRender
{
    public abstract class Matrix
    {
        public virtual int Row { get; }
        public virtual int Column { get; }

        public double this[int index]
        {
            get
            {
                return m[index];
            }
            set
            {
                m[index] = value;
            }
        }

        public double this[int row, int column]
        {
            get
            {
                return m[row * Column + column];
            }
            set
            {
                m[row * Column + column] = value;
            }
        }

        protected double[] m;
        protected StringBuilder stringBuilder = new StringBuilder();

        public Matrix()
        {
            m = new double[Row * Column];
            Array.Clear(m, 0, m.Length);
        }

        public override string ToString()
        {
            stringBuilder.Clear();
            stringBuilder.AppendLine(GetType().ToString());
            for (int row = 0; row < Row; row++)
            {
                stringBuilder.AppendLine(string.Join(" ", m.Skip(row * Column).Take(Column)));
            }
            return stringBuilder.ToString();
        }


        public Matrix(double[] m)
        {
            this.m = m;
        }

        public void SetValue(int index, double value)
        {
            m[index] = value;
        }

        public void SetValue(int c, int r, double value)
        {
            SetValue(r * Column + c, value);
        }

        public double GetValue(int index)
        {
            return m[index];
        }

        public double GetValue(int c, int r)
        {
            return GetValue(r * Column + c);
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            return m1.Multiply(m2);
        }

        public abstract Matrix Multiply(Matrix other);

        protected void ProcessMultiply(Matrix m2, ref Matrix m)
        {
            Matrix m1 = this;
            // Set value for new row and column of new matrix
            for (int row = 0; row < m1.Row; row++)
            {
                for (int column = 0; column < m2.Column; column++)
                {
                    // Calculate value
                    for (int index = 0; index < m1.Column; index++)
                    {
                        m[row * m2.Column + column] += m1[row, index] * m2[index, column];
                    }
                }
            }
        }
    }

    public class Matrix41 : Matrix
    {
        public override int Column
        {
            get
            {
                return 4;
            }
        }

        public override int Row
        {
            get
            {
                return 1;
            }
        }

        public Matrix41() : base() { }

        public Matrix41(double[] m) : base(m) { }

        public override Matrix Multiply(Matrix other)
        {
            Matrix m = new Matrix41();

            ProcessMultiply(other, ref m);

            return m;
        }
    }

    public class Matrix44 : Matrix
    {
        public override int Column
        {
            get
            {
                return 4;
            }
        }

        public override int Row
        {
            get
            {
                return 4;
            }
        }

        public Matrix44() : base() { }
        public Matrix44(double[] m) : base(m) { }

        public override Matrix Multiply(Matrix other)
        {
            Matrix m = new Matrix44();

            ProcessMultiply(other, ref m);

            return m;
        }

        public static Matrix44 Identity
        {
            get
            {
                return new Matrix44(new[]
                {
                    1.0, 0, 0 ,0,
                    0, 1.0, 0 ,0,
                    0, 0, 1.0, 0,
                    0, 0, 0, 1.0
                });
            }
        }

        public static Matrix44 Translate(double x, double y, double z)
        {
            return new Matrix44(new[]
            {
                    1.0, 0, 0 ,x,
                    0, 1.0, 0 ,y,
                    0, 0, 1.0, z,
                    0, 0, 0, 1.0
            });
        }

        public static Matrix44 Scale(double x, double y, double z)
        {
            return new Matrix44(new[]
            {
                    x, 0, 0 ,0,
                    0, y, 0 ,0,
                    0, 0, z, 0,
                    0, 0, 0, 1.0
            });
        }


        public static Matrix44 RotateX(double radAngle)
        {
            return new Matrix44(new[]
            {
                    1.0, 0, 0 ,0,
                    0, Math.Cos(radAngle), -Math.Sin(radAngle) ,0,
                    0, Math.Sin(radAngle), Math.Cos(radAngle), 0,
                    0, 0, 0, 1.0
            });
        }



        public static Matrix44 RotateY(double radAngle)
        {
            return new Matrix44(new[]
            {
                    Math.Cos(radAngle), 0, Math.Sin(radAngle) ,0,
                    0, 1.0, 0 ,0,
                    -Math.Sin(radAngle), 0, Math.Cos(radAngle), 0,
                    0, 0, 0, 1.0
            });
        }

        public static Matrix44 RotateZ(double radAngle)
        {
            return new Matrix44(new[]
            {
                    Math.Cos(radAngle), -Math.Sin(radAngle), 0 ,0,
                    Math.Sin(radAngle), Math.Cos(radAngle), 0 ,0,
                    0, 0, 1.0 , 0,
                    0, 0, 0, 1.0
            });
        }

        public static Matrix44 Rotation(double x, double y, double z)
        {
            return (RotateX(x) * RotateY(y) * RotateZ(z)) as Matrix44;
        }
    }
}
