﻿
using System;

namespace SharpRender
{
    public static class MathHelper
    {
        public static double ToRad(double deg)
        {
            return deg * Math.PI / 180.0;
        }

        public static double ToDeg(double rad)
        {
            return rad * 180.0f / Math.PI;
        }

        public static T Clamp<T>(T value, T minValue, T maxValue) where T : IComparable
        {
            if (value.CompareTo(minValue) < 0) return minValue;
            if (value.CompareTo(maxValue) > 0) return maxValue;
            return value;
        }
    }
}
