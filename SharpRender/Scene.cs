﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SharpRender
{
    public class Scene
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public Matrix44 Transform { get; private set; }
        public Matrix44 InverseTransform { get; private set; }

        private Color background;
        private Color[] colors;
        //private Color 
        private List<Object3D> object3Ds = new List<Object3D>();
        private List<LightSource> sources = new List<LightSource>();
        private double cameraX, cameraY, distance;
        private double Ka;

        public Scene(string filePath)
        {
            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                double width, height;
                double x, y, z;
                byte r, g, b;
                Matrix44 translationMatrix, scaleMatrix, rotationXMatrix, rotationYMatrix, rotationZMatrix;
                Matrix44 inverseTranslationMatrix, inverseScaleMatrix, inverseRotationXMatrix, inverseRotationYMatrix, inverseRotationZMatrix;
                Matrix44 rotationMatrix, inverseRotationMatrix;

                string line;
                string[] splitted;

                // First line
                // Back ground color in format: r g b
                line = reader.ReadLine();
                splitted = line.Split(' ');

                r = byte.Parse(splitted[0]);
                g = byte.Parse(splitted[1]);
                b = byte.Parse(splitted[2]);
                background = new Color(r, g, b);

                // Second line
                // Scene size in format: width height
                line = reader.ReadLine();
                splitted = line.Split(' ');

                width = int.Parse(splitted[0]);
                height = int.Parse(splitted[0]);
                Width = (int)width;
                Height = (int)height;

                // Calculate scale matrix
                scaleMatrix = Matrix44.Scale(width / 2.0, height / 2.0, 0);

                // Get inverse scale matrix
                inverseScaleMatrix = Matrix44.Scale(2.0 / width, 2.0 / height, 0);


                // Third line
                // Translation matrix
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                translationMatrix = Matrix44.Translate(x, y, z);

                inverseTranslationMatrix = Matrix44.Translate(-x, -y, -z);

                // Fourth line
                // Rotation matrix
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                x = MathHelper.ToRad(x);
                y = MathHelper.ToRad(y);
                z = MathHelper.ToRad(z);

                rotationMatrix = Matrix44.Rotation(x, y, z);

                // Revese rotation matrix
                inverseRotationMatrix = Matrix44.Rotation(-x, -y, -z);

                // Calculate the transform matrix
                Transform = (translationMatrix * rotationMatrix) as Matrix44;

                InverseTransform = (inverseRotationMatrix * inverseTranslationMatrix) as Matrix44;

                // Fifth line
                // Camera position
                line = reader.ReadLine();
                splitted = line.Split(' ');

                cameraX = double.Parse(splitted[0]);
                cameraY = double.Parse(splitted[1]);
                distance = double.Parse(splitted[2]);

                // Last line
                // Ka
                line = reader.ReadLine();
                splitted = line.Split(' ');

                Ka = double.Parse(splitted[0]);

                colors = new Color[(int)width * (int)height];
                for (int i = 0; i < colors.Length; i++)
                {
                    colors[i] = new Color(r, g, b);
                }
            }
        }

        public void AddObject(Object3D obj)
        {
            Console.WriteLine(obj);
            object3Ds.Add(obj);
        }

        public void AddSources(LightSource source)
        {
            sources.Add(source);
        }

        public void SetPixel(int x, int y, Color color)
        {
            colors[(-y + Height / 2) * Width + (x + Width / 2)] = color;
        }

        public void RenderAndSave(string outputPath)
        {
            Render();

            Bitmap output = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            var bitmapData = output.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.WriteOnly, output.PixelFormat);

            var colorData = colors.SelectMany(c => new byte[] { c.B, c.G, c.R }).ToArray();

            Marshal.Copy(colorData, 0, bitmapData.Scan0, colorData.Length);

            output.UnlockBits(bitmapData);

            output.Save(outputPath);
        }

        protected void Render()
        {
            double rayX, rayY, rayZ, normal;
            int w, h;
            Color colorFinal;

            // View point
            Matrix41 cameraPoint = new Matrix41(new[] {
                cameraX, cameraY, distance, 1
            });


            // Pour tous les pixels de l'ecran
            for (h = Height / 2; h > -Height / 2; h--) // Attention au sens de l'axe y!!!
            {
                for (w = -Width / 2; w < Width / 2; w++)
                {
                    rayX = w - cameraPoint.GetValue(0);
                    rayY = h - cameraPoint.GetValue(1);
                    rayZ = -cameraPoint.GetValue(2);

                    // Normalize
                    normal = Math.Sqrt(rayX * rayX + rayY * rayY + rayZ * rayZ);
                    rayX /= normal;
                    rayY /= normal;
                    rayZ /= normal;

                    // Put pixel on the screen is the final color <colorFinal>
                    colorFinal = Raytracing(cameraPoint, rayX, rayY, rayZ, 0, 1);

                    SetPixel(w, h, colorFinal);
                }
            }
        }



        protected Color Raytracing(Matrix41 cameraPoint,
                   double rayX, double rayY, double rayZ,
                   int recurseCount, double center)
        {

            byte r, g, b;
            byte ra, ga, ba;
            byte rs, gs, bs;
            byte rt, gt, bt;
            double ir = 0, ig = 0, ib = 0;
            Color colorFinal;
            Color localColor = background;
            Color reflx_col = new Color(0, 0, 0);
            Color trans_col = new Color(0, 0, 0);

            Color colorOutput = new Color();
            Color tmpColorOutput = new Color();
            Matrix41 tmp_M = new Matrix41();
            Matrix41 tmp_n = new Matrix41();
            Matrix41 M = new Matrix41();
            Matrix41 n = new Matrix41(); ;
            double tt;

            double min_distance, distance = 0;
            int k;

            double Kd = 0, Ks = 0, Kt = 0, Kn = 0;

            if (recurseCount < WorldSettings.MAX_RECURSION)
            {
                // Search the closest object (and therefore facet)

                // We initialize the nearest object no (-1) and
                // distance as -1
                min_distance = -1;
                foreach (var object3D in object3Ds)
                {
                    if (object3D.IntersectXYZ(cameraPoint, rayX, rayY, rayZ, ref distance,
                                 ref tmp_M, ref tmp_n, ref tmpColorOutput) > 0)
                    {
                        // Chercher le plus proche objet sur le rayon (E->(x,y,z))
                        if (min_distance == -1 || min_distance > distance)
                        {
                            min_distance = distance;
                            n = tmp_n;
                            M = tmp_M;
                            colorOutput = tmpColorOutput;
                            Kd = object3D.Kd;
                            Ks = object3D.Ks;
                            Kt = object3D.Kt;
                            Kn = object3D.Kn;
                        }
                    }
                }

                // Objet + proche trouve
                if (min_distance != -1)
                {
                    // Calculer Phong pour la couleur locale
                    localColor = Phong(cameraPoint, M, n, Kd, Ks, colorOutput);

                    // Coeff de reflexion??
                    if (Ks > 0)
                    {
                        Matrix41 M2 = new Matrix41();
                        Matrix41 Reflex = new Matrix41();
                        Matrix41 I = new Matrix41();
                        double x, y, z;

                        // Attention, ne pas oublier d'inverser le sens du rayon actuel!
                        I.SetValue(0, -rayX);
                        I.SetValue(1, -rayY);
                        I.SetValue(2, -rayZ);

                        // Calcul du vecteur reflechi (unitaire)
                        CalculateReflectionRay(I, n, ref Reflex);
                        x = Reflex.GetValue(0);
                        y = Reflex.GetValue(1);
                        z = Reflex.GetValue(2);

                        // Recuperation du parametre <tt> de l'equation de du rayon au
                        // point M
                        if (rayX != 0)
                        {
                            tt = (M.GetValue(0) - cameraPoint.GetValue(0)) / rayX;
                        }
                        else
                        if (rayY != 0)
                        {
                            tt = (M.GetValue(1) - cameraPoint.GetValue(1)) / rayY;
                        }
                        else
                        if (rayZ != 0)
                        {
                            tt = (M.GetValue(2) - cameraPoint.GetValue(2)) / rayZ;
                        }
                        else
                        {
                            // RAYON INEXISTANT (rx=ry=rz=0)!!!!
                            tt = 0;
                        }

                        // Recursivite sur ce nouveau rayon
                        M2.SetValue(0, rayX * (tt - WorldSettings.EPSILON) + cameraPoint.GetValue(0));
                        M2.SetValue(1, rayY * (tt - WorldSettings.EPSILON) + cameraPoint.GetValue(1));
                        M2.SetValue(2, rayZ * (tt - WorldSettings.EPSILON) + cameraPoint.GetValue(2));

                        reflx_col = Raytracing(M2, x, y, z, recurseCount + 1, center);
                    }

                    // Coeff de transmission?
                    if (Kt > 0)
                    {
                        Matrix41 M2 = new Matrix41();
                        Matrix41 Trans = new Matrix41();
                        Matrix41 I = new Matrix41();
                        double x, y, z;
                        double ni = center, nt = Kn;

                        // Attention, ne pas oublier d'inverser le sens du rayon actuel!
                        I.SetValue(0, -rayX);
                        I.SetValue(1, -rayY);
                        I.SetValue(2, -rayZ);

                        // Calcul du vecteur transmis (unitaire)
                        if (CalculateTransmitteRay(I, n, ref Trans, ni, nt) == 1)
                        {
                            // On sort de l'objet donc remettre le coeff du milieu a 1
                            Kn = 1;
                        }

                        x = Trans.GetValue(0);
                        y = Trans.GetValue(1);
                        z = Trans.GetValue(2);

                        // Recuperation du parametre <tt> de l'equation de du rayon au
                        // point M
                        if (rayX != 0)
                        {
                            tt = (M.GetValue(0) - cameraPoint.GetValue(0)) / rayX;
                        }
                        else
                        if (rayY != 0)
                        {
                            tt = (M.GetValue(1) - cameraPoint.GetValue(1)) / rayY;
                        }
                        else
                        if (rayZ != 0)
                        {
                            tt = (M.GetValue(2) - cameraPoint.GetValue(2)) / rayZ;
                        }
                        else
                        {
                            // RAYON INEXISTANT (rx=ry=rz=0)!!!!
                            tt = 0;
                        }

                        // Recursivite sur ce nouveau rayon
                        M2.SetValue(0, rayX * (tt + WorldSettings.EPSILON) + cameraPoint.GetValue(0));
                        M2.SetValue(1, rayY * (tt + WorldSettings.EPSILON) + cameraPoint.GetValue(1));
                        M2.SetValue(2, rayZ * (tt + WorldSettings.EPSILON) + cameraPoint.GetValue(2));

                        trans_col = Raytracing(M2, x, y, z, recurseCount + 1, Kn);
                    }

                    // Calcul de la couleur finale
                    ra = localColor.R;
                    ga = localColor.G;
                    ba = localColor.B;

                    rs = reflx_col.R;
                    gs = reflx_col.G;
                    bs = reflx_col.B;

                    rt = trans_col.R;
                    gt = trans_col.G;
                    bt = trans_col.B;

                    ir = (ra + Ks * rs + Kt * rt);
                    ig = (ga + Ks * gs + Kt * gt);
                    ib = (ba + Ks * bs + Kt * bt);
                }
                else
                {
                    // Calcul de la couleur finale (different ici car
                    // comme on n'a pas calculer Phong, il faut multiplier
                    // l'intensite locale par Ka)
                    ra = localColor.R;
                    ga = localColor.G;
                    ba = localColor.B;

                    rs = reflx_col.R;
                    gs = reflx_col.G;
                    bs = reflx_col.B;

                    rt = trans_col.R;
                    gt = trans_col.G;
                    bt = trans_col.B;

                    ir = (Ka * ra + Ks * rs + Kt * rt);
                    ig = (Ka * ga + Ks * gs + Kt * gt);
                    ib = (Ka * ba + Ks * bs + Kt * bt);
                }
            }

            // Remise a niveau des couleurs
            if (ir < 0)
                ir = 0;
            if (ig < 0)
                ig = 0;
            if (ib < 0)
                ib = 0;
            if (ir > 255)
                ir = 255;
            if (ig > 255)
                ig = 255;
            if (ib > 255)
                ib = 255;
            ir = MathHelper.Clamp(ir, 0, byte.MaxValue);
            ig = MathHelper.Clamp(ig, 0, byte.MaxValue);
            ib = MathHelper.Clamp(ib, 0, byte.MaxValue);

            r = (byte)ir;
            g = (byte)ig;
            b = (byte)ib;
            colorFinal = new Color(r, g, b);

            return colorFinal;
        }

        protected Color Phong(Matrix41 cameraPoint, Matrix41 M, Matrix41 n,
              double Kd, double Ks, Color output)
        {

            byte r, g, b;
            byte redOutput, greenOutput, blueOutput;
            byte red, green, blue;
            byte rsi, gsi, bsi;
            double ir, ig, ib;
            Color c;
            Matrix41 uu = new Matrix41();
            Matrix41 vv = new Matrix41();
            Matrix41 oo = new Matrix41();
            double uu_scal_n;
            double vv_scal_oo;
            Color local = background;
            Matrix41 pos_lum = new Matrix41();
            Color coul_lum;
            double x, y, z, norme;
            double Kd_r, Kd_v, Kd_b;       // <= Couleur de l'objet pour la diffusion
            int kk;

            red = local.R;
            green = local.G;
            blue = local.B;

            redOutput = output.R;
            greenOutput = output.G;
            blueOutput = output.B;

            ir = Ka * red * redOutput;
            ig = Ka * green * greenOutput;
            ib = Ka * blue * blueOutput;

            // Vecteur observateur (dans oo);
            x = cameraPoint.GetValue(0) - M.GetValue(0);
            y = cameraPoint.GetValue(1) - M.GetValue(1);
            z = cameraPoint.GetValue(2) - M.GetValue(2);
            norme = Math.Sqrt(x * x + y * y + z * z);
            x /= norme;
            y /= norme;
            z /= norme;

            oo[0] = x;
            oo[1] = y;
            oo[2] = z;

            for (kk = 0; kk < sources.Count; kk++)
            {
                // Couleur et positon de la lumiere
                coul_lum = sources[kk].Color;
                pos_lum = sources[kk].Position;

                // Vecteur incident (dans uu)
                x = pos_lum.GetValue(0) - M.GetValue(0);
                y = pos_lum.GetValue(1) - M.GetValue(1);
                z = pos_lum.GetValue(2) - M.GetValue(2);

                norme = Math.Sqrt(x * x + y * y + z * z);
                x /= norme;
                y /= norme;
                z /= norme;
                uu[0] = x;
                uu[1] = y;
                uu[2] = z;

                // Produit scalaire de uu avec n
                uu_scal_n = uu.GetValue(0) * n.GetValue(0) +
              uu.GetValue(1) * n.GetValue(1) +
              uu.GetValue(2) * n.GetValue(2);

                if (uu_scal_n < 0)
                    uu_scal_n = 0;

                // Vecteur reflechi (dans vv)
                CalculateReflectionRay(uu, n, ref vv);

                // Produit scalaire de vv avec oo
                vv_scal_oo = vv.GetValue(0) * oo.GetValue(0) +
              vv.GetValue(1) * oo.GetValue(1) +
              vv.GetValue(2) * oo.GetValue(2);

                // Attention, oo et vv doit etre dans le meme sens!!!
                // Sinon pas de reflexion...
                if (vv_scal_oo <= 0)
                {
                    vv_scal_oo = 0;
                }
                else
                {
                    // Cos alpha puissance n_phong
                    vv_scal_oo = Math.Pow(vv_scal_oo, WorldSettings.N_PHONG);
                }

                rsi = coul_lum.R;
                gsi = coul_lum.G;
                bsi = coul_lum.B;

                // Couleur de diffusion (*Kd <= le vrai coeff)
                Kd_r = Kd * redOutput / 255;
                Kd_v = Kd * greenOutput / 255;
                Kd_b = Kd * blueOutput / 255;

                ir += (Kd_r * uu_scal_n + Ks * vv_scal_oo) * rsi;
                ig += (Kd_v * uu_scal_n + Ks * vv_scal_oo) * gsi;
                ib += (Kd_b * uu_scal_n + Ks * vv_scal_oo) * bsi;
            }

            // Remise a niveau des couleurs
            if (ir < 0)
                ir = 0;
            if (ig < 0)
                ig = 0;
            if (ib < 0)
                ib = 0;

            ir = MathHelper.Clamp(ir, 0, byte.MaxValue);
            ig = MathHelper.Clamp(ig, 0, byte.MaxValue);
            ib = MathHelper.Clamp(ib, 0, byte.MaxValue);

            r = (byte)ir;
            g = (byte)ig;
            b = (byte)ib;

            return new Color(r, g, b);
        }


        protected void CalculateReflectionRay(Matrix41 I, Matrix41 n, ref Matrix41 R)
        {
            double I_scal_n;
            double x, y, z, normal;

            I_scal_n = I.GetValue(0) * n.GetValue(0) +
                     I.GetValue(1) * n.GetValue(1) +
                     I.GetValue(2) * n.GetValue(2);

            x = 2 * n.GetValue(0) * I_scal_n - I.GetValue(0);
            y = 2 * n.GetValue(1) * I_scal_n - I.GetValue(1);
            z = 2 * n.GetValue(2) * I_scal_n - I.GetValue(2);
            normal = Math.Sqrt(x * x + y * y + z * z);
            x /= normal;
            y /= normal;
            z /= normal;

            R.SetValue(0, x);
            R.SetValue(1, y);
            R.SetValue(2, z);
            R.SetValue(3, 1);
        }

        protected int CalculateTransmitteRay(Matrix41 I, Matrix41 n, ref Matrix41 T,
                      double ni, double nt)
        {
            double I_scal_n;
            double x, y, z, norme;
            double nn = ni / nt;

            I_scal_n = I.GetValue(0) * n.GetValue(0) +
                     I.GetValue(1) * n.GetValue(1) +
                     I.GetValue(2) * n.GetValue(2);

            if (I_scal_n < 0)
            {
                T.SetValue(0, n.GetValue(0));
                T.SetValue(1, n.GetValue(1));
                T.SetValue(2, n.GetValue(2));

                return (1); // On sort de l'objet
            }

            x = (nn * I_scal_n - Math.Sqrt(1 - nn * nn * (1 - I_scal_n * I_scal_n))) * n.GetValue(0) -
              nn * I.GetValue(0);
            y = (nn * I_scal_n - Math.Sqrt(1 - nn * nn * (1 - I_scal_n * I_scal_n))) * n.GetValue(1) -
              nn * I.GetValue(1);
            z = (nn * I_scal_n - Math.Sqrt(1 - nn * nn * (1 - I_scal_n * I_scal_n))) * n.GetValue(2) -
              nn * I.GetValue(2);

            norme = Math.Sqrt(x * x + y * y + z * z);
            x /= norme;
            y /= norme;
            z /= norme;

            T.SetValue(0, x);
            T.SetValue(1, y);
            T.SetValue(2, z);

            // Inside object
            return 0;
        }
    }
}
