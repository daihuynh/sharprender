﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpRender
{
    public class LightSource
    {
        public Matrix41 Position { get; private set; }
        
        public Color Color { get; private set; }

        private Scene scene;

        public LightSource(string filePath, Scene scene)
        {
            byte r, g, b;
            double x, y, z;
            this.scene = scene;

            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                string line;
                string[] splitted;

                // Read color
                line = reader.ReadLine();
                splitted = line.Split(' ');

                r = byte.Parse(splitted[0]);
                g = byte.Parse(splitted[1]);
                b = byte.Parse(splitted[2]);

                Color = new Color(r, g, b);

                // Read translation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                Position = new Matrix41(new[]
                {
                    x, y, z, 1
                });

                Position = (Position * scene.InverseTransform) as Matrix41;
            }
        }

        public LightSource(double x, double y, double z, Color c, Scene e)
        {
            this.scene = e;
            Color = c;

            SetPosition(x, y, z);
        }

        public void SetPosition(double x, double y, double z)
        {
            Position[0] = x;
            Position[1] = y;
            Position[2] = z;
            Position[3] = 1;
        }
    }
}
