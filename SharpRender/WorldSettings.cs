﻿namespace SharpRender
{
    public static class WorldSettings
    {
        public const int N_PHONG = 16;
        public const double EPSILON = 0.0001;
        public const int MAX_RECURSION = 10;
    }
}
