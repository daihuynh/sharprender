﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpRender
{
    class Program
    {
        static void Main(string[] args)
        {
            //var files = Directory.GetFiles(args[0]);
            var files = Directory.GetFiles("1");

            var sceneFile = files.First(f => f.ToLower().Contains("scene"));
            var scene = new Scene(sceneFile);


            var lightFiles = files.Where(f => f.ToLower().Contains("light"));
            foreach (var lightFile in lightFiles)
            {
                scene.AddSources(new LightSource(lightFile, scene));
            }

            var planFiles = files.Where(f => f.ToLower().Contains("plane"));
            foreach (var planFile in planFiles)
            {
                scene.AddObject(new Plane(planFile, scene));
            }

            var sphereFiles = files.Where(f => f.ToLower().Contains("sphere"));
            foreach (var sphereFile in sphereFiles)
            {
                scene.AddObject(new Sphere(sphereFile, scene));
            }

            var cubeFiles = files.Where(f => f.ToLower().Contains("cube"));
            foreach (var cubeFile in cubeFiles)
            {
                scene.AddObject(new Cube(cubeFile, scene));
            }

            var triangleFiles = files.Where(f => f.ToLower().Contains("triangle"));
            foreach (var triangleFile in triangleFiles)
            {
                scene.AddObject(new Triangle(triangleFile, scene));
            }

            scene.RenderAndSave("test.jpg");

            Console.WriteLine("DONE");
            Console.Read();
        }
    }
}
