﻿using System.IO;

namespace SharpRender
{
    public class Cube : Object3D
    {
        private Triangle[] tris = new Triangle[12];

        public Cube(string filePath, Scene scene)
        {
            byte r, g, b;
            double x, y, z;
            Matrix44 translationMatrix, scalingMatrix;
            Matrix44 inverseTranslationMatrix, inverseScalingMatrix;
            Matrix41 p1, p2, p3, p4, p5, p6, p7, p8;
            Matrix44 rotationMatrix, inverseRotationMatrix;

            this.Scene = scene;

            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                string line;
                string[] splitted;
                // Read color
                line = reader.ReadLine();
                splitted = line.Split(' ');

                r = byte.Parse(splitted[0]);
                g = byte.Parse(splitted[1]);
                b = byte.Parse(splitted[2]);

                Color = new Color(r, g, b);

                // Read translation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                translationMatrix = Matrix44.Translate(x, y, z);

                // inverse translation
                inverseTranslationMatrix = Matrix44.Translate(-x, -y, -z);

                // Read scale
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                scalingMatrix = Matrix44.Scale(x / 2.0, y / 2.0, z / 2.0);

                // Inverse scale
                inverseScalingMatrix = Matrix44.Scale(2.0 / x, 2.0 / y, 2.0 / z);

                // Lecture rotation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                x = MathHelper.ToRad(x);
                y = MathHelper.ToRad(y);
                z = MathHelper.ToRad(z);

                rotationMatrix = Matrix44.Rotation(x, y, z);

                // Inverse transform
                inverseRotationMatrix = Matrix44.Rotation(-x, -y, -z);

                // Lecture des coefficient Kd, Ks, Kt et Kn (coeff du milieu)
                line = reader.ReadLine();
                splitted = line.Split(' ');

                Kd = double.Parse(splitted[0]);
                Ks = double.Parse(splitted[1]);
                Kt = double.Parse(splitted[2]);
                Kn = double.Parse(splitted[3]);

                // Calcul de la matrice de transformation
                Transform = (translationMatrix * rotationMatrix * scalingMatrix) as Matrix44;
                // et son inverse
                InverseTransform = (inverseScalingMatrix * inverseRotationMatrix * inverseTranslationMatrix) as Matrix44;

                p1 = new Matrix41(new[] {
                    -1.0, 1.0, 1.0, 1.0
                });

                p2 = new Matrix41(new[] {
                    1.0, 1.0, 1.0, 1.0
                });

                p3 = new Matrix41(new[]
                {
                    1.0, -1.0, 1.0, 1.0
                });

                p4 = new Matrix41(new[]
                {
                    -1.0, -1.0, 1.0, 1.0
                });

                p5 = new Matrix41(new[]
                {
                    -1.0, 1.0, -1.0, 1.0
                });

                p6 = new Matrix41(new[]
                {
                    1.0, 1.0, -1.0, 1.0
                });

                p7 = new Matrix41(new[]
                {
                    1.0, -1.0, -1.0, 1.0
                });

                p8 = new Matrix41(new[]
                {
                    -1.0, -1.0, -1.0, 1.0
                });

                // Transformation des points du triangle (ils seront dans le repere canonique
                // de l'ecran (qui n'est pas le carre canonique mais le rectangle de la
                // taille de l'ecran centre a l'origine))
                p1 = (p1 * Transform) as Matrix41;
                p2 = (p2 * Transform) as Matrix41;
                p3 = (p3 * Transform) as Matrix41;
                p4 = (p4 * Transform) as Matrix41;
                p5 = (p5 * Transform) as Matrix41;
                p6 = (p6 * Transform) as Matrix41;
                p7 = (p7 * Transform) as Matrix41;
                p8 = (p8 * Transform) as Matrix41;

                p1 = (p1 * scene.InverseTransform) as Matrix41;
                p2 = (p2 * scene.InverseTransform) as Matrix41;
                p3 = (p3 * scene.InverseTransform) as Matrix41;
                p4 = (p4 * scene.InverseTransform) as Matrix41;
                p5 = (p5 * scene.InverseTransform) as Matrix41;
                p6 = (p6 * scene.InverseTransform) as Matrix41;
                p7 = (p7 * scene.InverseTransform) as Matrix41;
                p8 = (p8 * scene.InverseTransform) as Matrix41;


                tris[0] = new Triangle();
                tris[0].Create(scene, p1, p2, p4, Color);

                tris[1] = new Triangle();
                tris[1].Create(scene, p2, p3, p4, Color);

                tris[2] = new Triangle();
                tris[2].Create(scene, p5, p1, p8, Color);

                tris[3] = new Triangle();
                tris[3].Create(scene, p1, p4, p8, Color);

                tris[4] = new Triangle();
                tris[4].Create(scene, p2, p6, p3, Color);

                tris[5] = new Triangle();
                tris[5].Create(scene, p6, p7, p3, Color);

                tris[6] = new Triangle();
                tris[6].Create(scene, p5, p6, p1, Color);

                tris[7] = new Triangle();
                tris[7].Create(scene, p6, p2, p1, Color);

                tris[8] = new Triangle();
                tris[8].Create(scene, p4, p7, p8, Color);

                tris[9] = new Triangle();
                tris[9].Create(scene, p4, p3, p7, Color);

                tris[10] = new Triangle();
                tris[10].Create(scene, p8, p6, p5, Color);

                tris[11] = new Triangle();
                tris[11].Create(scene, p8, p7, p6, Color);
            }
        }


        public override int IntersectXYZ(Matrix41 E,
                    double rx, double ry, double rz,
                    ref double distance, ref Matrix41 M,
                    ref Matrix41 n, ref Color c)
        {
            Matrix41 tmp_M = new Matrix41();
            Matrix41 tmp_n = new Matrix41();
            Color tmp_c = new Color();
            double min_distance = -1;
            /*
              On teste l'intersection avec toutes les
              facettes du cube pour recuperer celle la plus proche
            */
            for (int i = 0; i < 12; i++)
            {
                if (tris[i].IntersectXYZ(E, rx, ry, rz, ref distance, ref tmp_M, ref tmp_n,
                            ref tmp_c) > 0)
                {
                    if (min_distance == -1 || min_distance > distance)
                    {
                        min_distance = distance;
                        M.SetValue(0, tmp_M.GetValue(0));
                        M.SetValue(1, tmp_M.GetValue(1));
                        M.SetValue(2, tmp_M.GetValue(2));
                        n = tmp_n;
                        c = tmp_c;
                    }
                }
            }

            // Intersecte avec succes
            if (min_distance != -1)
            {
                distance = min_distance;
                return 1;
            }

            return 0;
        }
    }

}
