﻿using System;
using System.IO;

namespace SharpRender
{

    public class Plane : Object3D
    {
        private Triangle[] tris = new Triangle[2];



        public Plane(string filePath, Scene scene)
        {
            byte r, g, b;
            double x, y, z;
            Matrix44 translationMatrix, scalingMatrix;
            Matrix44 inverseTranslationMatrix, inverseScalingMatrix;
            Matrix41 p1, p2, p3, p4;
            Matrix44 rotationMatrix, inverseRotationMatrix;

            this.Scene = scene;

            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                string line;
                string[] splitted;
                // Read color
                line = reader.ReadLine();
                splitted = line.Split(' ');

                r = byte.Parse(splitted[0]);
                g = byte.Parse(splitted[1]);
                b = byte.Parse(splitted[2]);

                Color = new Color(r, g, b);

                // Read translation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                translationMatrix = Matrix44.Translate(x, y, z);

                // inverse translation
                inverseTranslationMatrix = Matrix44.Translate(-x, -y, -z);

                // Read scale
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                scalingMatrix = Matrix44.Scale(x / 2.0, y / 2.0, z / 2.0);

                // Inverse scale
                inverseScalingMatrix = Matrix44.Scale(2.0 / x, 2.0 / y, 2.0 / z);

                // Lecture rotation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                x = MathHelper.ToRad(x);
                y = MathHelper.ToRad(y);
                z = MathHelper.ToRad(z);

                rotationMatrix = Matrix44.Rotation(x, y, z);

                // Inverse transform
                inverseRotationMatrix = Matrix44.Rotation(-x, -y, -z);

                // Lecture des coefficient Kd, Ks, Kt et Kn (coeff du milieu)
                line = reader.ReadLine();
                splitted = line.Split(' ');

                Kd = double.Parse(splitted[0]);
                Ks = double.Parse(splitted[1]);
                Kt = double.Parse(splitted[2]);
                Kn = double.Parse(splitted[3]);

                // Calcul de la matrice de transformation
                Transform = (translationMatrix * rotationMatrix * scalingMatrix) as Matrix44;
                // et son inverse
                InverseTransform = (inverseScalingMatrix * inverseRotationMatrix * inverseTranslationMatrix) as Matrix44;

                p1 = new Matrix41(new[] {
                    -1.0, 1.0, 0.0, 1.0
                });

                p2 = new Matrix41(new[] {
                    1.0, 1.0, 0.0, 1.0
                });

                p3 = new Matrix41(new[]
                {
                    1.0, -1.0, 0.0, 1.0
                });

                p4 = new Matrix41(new[]
                {
                    -1.0, -1.0, 0.0, 1.0
                });

                // Transformation des points du triangle (ils seront dans le repere canonique
                // de l'ecran (qui n'est pas le carre canonique mais le rectangle de la
                // taille de l'ecran centre a l'origine))
                p1 = (p1 * Transform) as Matrix41;
                p2 = (p2 * Transform) as Matrix41;
                p3 = (p3 * Transform) as Matrix41;
                p4 = (p4 * Transform) as Matrix41;

                p1 = (p1 * scene.InverseTransform) as Matrix41;
                p2 = (p2 * scene.InverseTransform) as Matrix41;
                p3 = (p3 * scene.InverseTransform) as Matrix41;
                p4 = (p4 * scene.InverseTransform) as Matrix41;

                tris[0] = new Triangle();
                tris[0].Create(scene, p1, p2, p4, Color);
                tris[1] = new Triangle();
                tris[1].Create(scene, p2, p3, p4, Color);
            }
        }

        public override int IntersectXYZ(Matrix41 E,
                    double rx, double ry, double rz,
                    ref double distance, ref Matrix41 M,
                    ref Matrix41 n, ref Color c)
        {
            Color tmp_c = new Color();
            Matrix41 tmp_n = new Matrix41();
            Matrix41 tmp_M = new Matrix41();
            double min_distance = -1;
            int i_facette = -1;
            /*
              On teste l'intersection avec toutes les
              facettes du carre pour trouver le plus proche
            */
            for (int i = 0; i < 2; i++)
            {
                if (tris[i].IntersectXYZ(E, rx, ry, rz, ref distance, ref tmp_M, ref tmp_n,
                            ref tmp_c) > 0)
                {
                    if (min_distance == -1 || min_distance > distance)
                    {
                        min_distance = distance;
                        i_facette = i;
                        M.SetValue(0, tmp_M.GetValue(0));
                        M.SetValue(1, tmp_M.GetValue(1));
                        M.SetValue(2, tmp_M.GetValue(2));
                        n = tmp_n;
                        c = tmp_c;
                    }
                }
            }

            // Intersecte avec succes
            if (min_distance != -1)
            {
                distance = min_distance;
                return (1);
            }

            return (0);
        }
    }
}
