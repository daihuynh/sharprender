﻿using System;
using System.IO;

namespace SharpRender
{
    public class Sphere : Object3D
    {
        public Sphere(string filePath, Scene scene)
        {
            byte r, g, b;
            double x, y, z;
            Matrix44 translationMatrix, scalingMatrix;
            Matrix44 inverseTranslationMatrix, inverseScalingMatrix;
            Matrix44 rotationMatrix, inverseRotationMatrix;

            this.Scene = scene;

            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                string line;
                string[] splitted;
                // Read color
                line = reader.ReadLine();
                splitted = line.Split(' ');

                r = byte.Parse(splitted[0]);
                g = byte.Parse(splitted[1]);
                b = byte.Parse(splitted[2]);

                Color = new Color(r, g, b);

                // Read translation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                translationMatrix = Matrix44.Translate(x, y, z);

                // inverse translation
                inverseTranslationMatrix = Matrix44.Translate(-x, -y, -z);

                // Read scale
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                scalingMatrix = Matrix44.Scale(x / 2.0, y / 2.0, z / 2.0);

                // Inverse scale
                inverseScalingMatrix = Matrix44.Scale(2.0 / x, 2.0 / y, 2.0 / z);

                // Lecture rotation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                x = MathHelper.ToRad(x);
                y = MathHelper.ToRad(y);
                z = MathHelper.ToRad(z);

                rotationMatrix = Matrix44.Rotation(x, y, z);

                // Inverse transform
                inverseRotationMatrix = Matrix44.Rotation(-x, -y, -z);

                // Calcul de la matrice de transformation
                Transform = (translationMatrix * rotationMatrix * scalingMatrix) as Matrix44;

                // et son inverse
                InverseTransform = (inverseScalingMatrix * inverseRotationMatrix * inverseTranslationMatrix) as Matrix44;

                // Lecture des coefficient Kd, Ks, Kt et Kn (coeff du milieu)
                line = reader.ReadLine();
                splitted = line.Split(' ');

                Kd = double.Parse(splitted[0]);
                Ks = double.Parse(splitted[1]);
                Kt = double.Parse(splitted[2]);
                Kn = double.Parse(splitted[3]);
            }
        }

        public override int IntersectXYZ(Matrix41 cameraPoint,
                    double rx, double ry, double rz,
                    ref double distance, ref Matrix41 M,
                    ref Matrix41 n, ref Color c)
        {
            double b1, b2, b3;
            double t1, t2, delta;
            double x, y, z;
            double x2, y2, z2, normal;
            double aa, bb, cc;
            double tt;
            int choix;
            Matrix41 toto = new Matrix41();
            Matrix41 cameraPoint2 = new Matrix41();

            // Calculate the transformation matrix to reduce to the repere
            // Canonical sphere (We must therefore apply the transformation matrix
            // The screen (because it is assumed canonical repere of the screen) and then applies
            // The inverse of the object transformation matrix (sphere). this is
            // lThe <m2_inv> matrix <m> its inverse.
            Matrix44 m2 = (Scene.InverseTransform * Transform) as Matrix44;
            Matrix44 m2_inv = (InverseTransform * Scene.Transform) as Matrix44;

            cameraPoint[3] = 1;

            Matrix41 transform2 = new Matrix41(new[] {
                cameraPoint[0] + rx,
                cameraPoint[1] + ry,
                cameraPoint[2] + rz,
                1
            });

            // Transformation de F2
            transform2 = (transform2 * m2_inv) as Matrix41;

            cameraPoint2 = cameraPoint;
            // Transformation de E2
            cameraPoint2 = (cameraPoint2 * m2_inv) as Matrix41;


            rx = transform2.GetValue(0) - cameraPoint2.GetValue(0);
            ry = transform2.GetValue(1) - cameraPoint2.GetValue(1);
            rz = transform2.GetValue(2) - cameraPoint2.GetValue(2);
            normal = Math.Sqrt(rx * rx + ry * ry + rz * rz);

            if (normal == 0) // PAS DE RAYON!!
                return (0);

            rx /= normal;
            ry /= normal;
            rz /= normal;


            b1 = cameraPoint2.GetValue(0);
            b2 = cameraPoint2.GetValue(1);
            b3 = cameraPoint2.GetValue(2);

            if (b3 == 0)
                return 0;

            // See the report or the market price for explanations ...

            aa = rx * rx + ry * ry + rz * rz;
            bb = 2 * (rx * b1 + ry * b2 + rz * b3);
            cc = b1 * b1 + b2 * b2 + b3 * b3 - 1; // 1 = radius of the canonical sphere (1 * 1 = 1)

            delta = bb * bb - 4 * aa * cc;

            if (delta < 0)
                return (0);

            if (aa == 0)
                return (0);

            delta = Math.Sqrt(delta);

            t1 = (-bb + delta) / (2 * aa);
            t2 = (-bb - delta) / (2 * aa);

            if (t1 <= 0 && t2 <= 0)
                return (0);

            if (t1 <= 0)
                choix = 2;
            else
              if (t2 <= 0)
                choix = 1;
            else
                if (t1 < t2)
                choix = 1;
            else
                choix = 2;

            if (choix == 1)
            {
                tt = t1;
            }
            else
            {
                tt = t2;
            }

            // Calcul du point M (intersection)
            x = rx * tt + b1;
            y = ry * tt + b2;
            z = rz * tt + b3;

            toto.SetValue(0, x);
            toto.SetValue(1, y);
            toto.SetValue(2, z);
            toto.SetValue(3, 1);
            // Transformation
            toto = (toto * m2) as Matrix41;

            x2 = toto.GetValue(0);
            y2 = toto.GetValue(1);
            z2 = toto.GetValue(2);

            M.SetValue(0, x2);
            M.SetValue(1, y2);
            M.SetValue(2, z2);
            M.SetValue(3, 1);

            toto.SetValue(0, 0);
            toto.SetValue(1, 0);
            toto.SetValue(2, 0);
            toto.SetValue(3, 1);
            toto = (toto * Transform) as Matrix41;
            toto = (toto * Scene.InverseTransform) as Matrix41;

            x2 = M.GetValue(0) - toto.GetValue(0);
            y2 = M.GetValue(1) - toto.GetValue(1);
            z2 = M.GetValue(2) - toto.GetValue(2);

            normal = Math.Sqrt(x2 * x2 + y2 * y2 + z2 * z2);
            x2 /= normal;
            y2 /= normal;
            z2 /= normal;
            n.SetValue(0, x2);
            n.SetValue(1, y2);
            n.SetValue(2, z2);
            n.SetValue(3, 1);


            // Distance de E a M
            x2 = M.GetValue(0) - cameraPoint.GetValue(0);
            y2 = M.GetValue(1) - cameraPoint.GetValue(1);
            z2 = M.GetValue(2) - cameraPoint.GetValue(2);
            distance = Math.Sqrt(x2 * x2 + y2 * y2 + z2 * z2);


            // Couleur de l'objet actuel
            c = new Color(Color.R, Color.G, Color.B);

            return 1;
        }
    }

}
