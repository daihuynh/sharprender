﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpRender
{
    public abstract class Object3D
    {
        public double Kd { get; set; } // Diffuse
        public double Ks { get; set; } // Specular
        public double Kt { get; set; }
        public double Kn { get; set; }

        public Matrix44 Transform { get; set; }
        public Matrix44 InverseTransform { get; set; }
        public Color Color { get; set; }
        public Scene Scene { get; set; }
        public Matrix41[] Vertices { get; protected set; }
        public Matrix41 Normal { get; protected set; }

        public abstract int IntersectXYZ(Matrix41 cameraPoint,
                  double rx, double ry, double rz,
                    ref double distance, ref Matrix41 M,
                    ref Matrix41 n, ref Color c);
    }
}
