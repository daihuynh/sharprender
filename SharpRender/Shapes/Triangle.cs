﻿using System;
using System.IO;

namespace SharpRender
{
    public class Triangle : Object3D
    {
        public Triangle()
        {
            base.Vertices = new Matrix41[3];
        }

        public Triangle(string filePath, Scene scene) : this()
        {
            byte r, g, b;
            double x, y, z;
            Matrix44 translationMatrix, scalingMatrix;
            Matrix44 inverseTranslationMatrix, inverseScalingMatrix;
            Matrix44 rotationMatrix, inverseRotationMatrix;
            Matrix41 p1, p2, p3;

            this.Scene = scene;

            using (var reader = new StreamReader(File.Open(filePath, FileMode.Open)))
            {
                string line;
                string[] splitted;
                // Read color
                line = reader.ReadLine();
                splitted = line.Split(' ');

                r = byte.Parse(splitted[0]);
                g = byte.Parse(splitted[1]);
                b = byte.Parse(splitted[2]);

                Color = new Color(r, g, b);

                // Read translation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                translationMatrix = Matrix44.Translate(x, y, z);

                // inverse translation
                inverseTranslationMatrix = Matrix44.Translate(-x, -y, -z);

                // Read scale
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                scalingMatrix = Matrix44.Scale(x / 2.0, y / 2.0, z / 2.0);

                // Inverse scale
                inverseScalingMatrix = Matrix44.Scale(2.0 / x, 2.0 / y, 2.0 / z);

                // Lecture rotation
                line = reader.ReadLine();
                splitted = line.Split(' ');

                x = double.Parse(splitted[0]);
                y = double.Parse(splitted[1]);
                z = double.Parse(splitted[2]);

                x = MathHelper.ToRad(x);
                y = MathHelper.ToRad(y);
                z = MathHelper.ToRad(z);

                rotationMatrix = Matrix44.Rotation(x, y, z);

                // Inverse transform
                inverseRotationMatrix = Matrix44.Rotation(-x, -y, -z);

                // Lecture des coefficient Kd, Ks, Kt et Kn (coeff du milieu)
                line = reader.ReadLine();
                splitted = line.Split(' ');

                Kd = double.Parse(splitted[0]);
                Ks = double.Parse(splitted[1]);
                Kt = double.Parse(splitted[2]);
                Kn = double.Parse(splitted[3]);

                // Calcul de la matrice de transformation
                Transform = (translationMatrix * rotationMatrix * scalingMatrix) as Matrix44;
                // et son inverse
                InverseTransform = (inverseScalingMatrix * inverseRotationMatrix * inverseTranslationMatrix) as Matrix44;

                // Initialisation des points du triangle dans le repere canonique
                // Voici sa forme par defaut :
                //           ______
                //           |    /
                //           |   /
                //           |  /
                //           | /
                //           |/
                //           '

                p1 = new Matrix41(new[] {
                    -1.0, 1.0, 0.0, 1.0
                });

                p2 = new Matrix41(new[] {
                    1.0, 1.0, 0.0, 1.0
                });

                p3 = new Matrix41(new[]
                {
                    -1.0, -1.0, 0.0, 1.0
                });

                // Transformation des points du triangle (ils seront dans le repere canonique
                // de l'ecran (qui n'est pas le carre canonique mais le rectangle de la
                // taille de l'ecran centre a l'origine))
                p1 = (p1 * Transform) as Matrix41;
                p2 = (p2 * Transform) as Matrix41;
                p3 = (p3 * Transform) as Matrix41;

                p1 = (p1 * scene.InverseTransform) as Matrix41;
                p2 = (p2 * scene.InverseTransform) as Matrix41;
                p3 = (p3 * scene.InverseTransform) as Matrix41;

                // Triangularisation
                Create(scene, p1, p2, p3, Color);
            }
        }

        public void Create(Scene scene, Matrix41 p1, Matrix41 p2, Matrix41 p3, Color c)
        {
            Vertices[0] = p1;
            Vertices[1] = p2;
            Vertices[2] = p3;

            Scene = scene;
            Color = c;
            CalculateNormal();
        }

        protected void CalculateNormal()
        {
            double x1, y1, z1, x2, y2, z2;
            double cx, cy, cz;

            x1 = Vertices[2][0] - Vertices[0][0];
            y1 = Vertices[2][1] - Vertices[0][1];
            z1 = Vertices[2][2] - Vertices[0][2];

            x2 = Vertices[1][0] - Vertices[0][0];
            y2 = Vertices[1][1] - Vertices[0][1];
            z2 = Vertices[1][2] - Vertices[0][2];

            // C'est un simple produit vectoriel
            cx = y1 * z2 - z1 * y2;
            cy = z1 * x2 - x1 * z2;
            cz = x1 * y2 - y1 * x2;

            // Ne pas oublier de le norme
            x1 = Math.Sqrt(cx * cx + cy * cy + cz * cz);

            Normal = new Matrix41(new[]
            {
                cx / x1, cy / x1, cz / x1, 0
            });
        }

        public override int IntersectXYZ(Matrix41 E,
                    double x3, double y3, double z3,
                    ref double distance, ref Matrix41 M,
                    ref Matrix41 n, ref Color c)
        {
            double D, u, v, t;
            double x1, y1, z1;
            double x2, y2, z2;
            double xa, ya, za;
            double xp, yp, zp;

            // Voir explications dans le rapport ou dans le cours...

            x1 = Vertices[1].GetValue(0) - Vertices[0].GetValue(0);
            y1 = Vertices[1].GetValue(1) - Vertices[0].GetValue(1);
            z1 = Vertices[1].GetValue(2) - Vertices[0].GetValue(2);

            x2 = Vertices[2].GetValue(0) - Vertices[0].GetValue(0);
            y2 = Vertices[2].GetValue(1) - Vertices[0].GetValue(1);
            z2 = Vertices[2].GetValue(2) - Vertices[0].GetValue(2);

            xa = Vertices[0].GetValue(0);
            ya = Vertices[0].GetValue(1);
            za = Vertices[0].GetValue(2);

            xp = E.GetValue(0);
            yp = E.GetValue(1);
            zp = E.GetValue(2);


            D = -z1 * y3 * x2 + y1 * z3 * x2 + x3 * z1 * y2 + y3 * x1 * z2 - z3 * x1 * y2 - x3 * y1 * z2;
            if (D == 0)
                return (0);

            u = -(-y3 * x2 * za - y3 * xp * z2 + y3 * x2 * zp + y3 * xa * z2 + x3 * y2 * za - x3 * y2 * zp + x2 * z3 * ya - x2 * z3 * yp
                + xp * z3 * y2 + x3 * yp * z2 - x3 * ya * z2 - xa * z3 * y2) / D;
            if (u < 0)
                return (0);

            v = (-x3 * z1 * ya - x3 * y1 * zp + x3 * z1 * yp + x3 * y1 * za + z3 * x1 * ya + y1 * z3 * xp + z1 * y3 * xa - z3 * x1 * yp - y3 * x1 * za
               - y1 * z3 * xa + y3 * x1 * zp - z1 * y3 * xp) / D;
            if (v < 0)
                return (0);

            if ((u + v) > 1)
                return (0);

            t = (-xp * z1 * y2 + xp * y1 * z2 + xa * z1 * y2 - xa * y1 * z2 - x1 * y2 * za + x1 * y2 * zp - x1 * yp * z2 + x1 * ya * z2 - x2 * z1 * ya
               - x2 * y1 * zp + x2 * z1 * yp + x2 * y1 * za) / D;

            if (t <= 0)
                return (0);

            x1 = x3 * t;
            y1 = y3 * t;
            z1 = z3 * t;

            // Coord de M (point d'intersection)
            M.SetValue(0, x1 + xp);
            M.SetValue(1, y1 + yp);
            M.SetValue(2, z1 + zp);

            // Calcul de la distance entre E et M
            distance = Math.Sqrt(x1 * x1 + y1 * y1 + z1 * z1);

            // Couleur de la facette
            c = Color;

            // Normale de la facette
            n = Normal;

            // Intersecte avec succes
            return 1;
        }
    }

}
